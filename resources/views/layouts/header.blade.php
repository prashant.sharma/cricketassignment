<!DOCTYPE html>
<html lang="en">
<head>
  <title>2020-Champions Trophy</title>
  <meta charset="utf-8">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
  <h2>2020-Champions Trophy</h2>
  <p></p>  

<ul class="nav nav-tabs">
<li class="active"><a data-toggle="tab" href="#teams">Teams</a></li>
<li><a data-toggle="tab" href="#pointstable">Points Table</a></li>
<li><a data-toggle="tab" href="#total_match">Total Match</a></li>
  <li ><a data-toggle="tab" href="#innings">Matches Fixtures</a></li>
  
  
</ul>