@extends('layouts.master')
@section('content')
<div class="tab-content">
<!-- for team--->
<div id="teams" class="tab-pane fade in active">
  <table class="table">
    <thead>
      <tr>
        <th>Teams</th>
      </tr>
    </thead>
    <tbody>
    @foreach($teams as $key=>$val)
      <tr>
        <td> <image src="{{ asset('uploads').'/'.$val->logoUri }}" style="width: 16px;"> {{ $val->name }}
        <br>

        <?php
        $players = \App\Players::where('teamsId', $val->id)->get();
        ?>
        @foreach($players as $keyPlayer=>$valuePlayer)
        <span> {{ $keyPlayer + 1 }} : Player Name: {{ $valuePlayer->firstName ?? '' }} {{ $valuePlayer->lastName ?? '' }} (Jersey Number : {{ $valuePlayer->jerseyNumber }})</span> <br>
        @endforeach
        </td>
      </tr>
      
      @endforeach
      

    </tbody>
  </table>

  </div>
  <!-- for team end--->

 <!-- for points table --->
  <div id="pointstable" class="tab-pane fade">
  <!-- <div><a>Main</a></div><div><a onclick="return play_off();">Play Offs</a></div> -->
  <h4>Points Table</h4>
  <table class="table">

    <thead>
      <tr style="background-color: #170101;">
        <th style="color: white;">Team List</th>
        <th style="color: white;">Win</th>
        <th style="color: white;">Loss</th>
      </tr>
    </thead>
    <tbody>
    @foreach($teams as $data)
    <?php
    $wincount = \App\Match::where('win_team', $data->id)->count();
    $losscount = \App\Match::where('loss_team', $data->id)->count();
    
    ?>
      <tr>
        <td >{{ $data->name }}</td>
        <td>{{ $wincount ?? '0' }}</td>
        <td>{{ $losscount ?? '0' }}</td>
      </tr>
      @endforeach
    </tbody>
<hr>

   
  </table>
  </div>
<!-- end points table --->


<!-- for points table --->
<div id="innings" class="tab-pane fade">
  <h4>Innings</h4>
  <table class="table">

    <thead>
      <tr style="background-color: #170101;">
        <th style="color: white;">Play Match (From)</th>
        <th style="color: white;">Play Match (To)</th>
        <th style="color: white;"></th>
      </tr>
    </thead>
    <tbody>
    
      <tr>
        <td >
        <select class="form-control" id="fromteam">
        <option value="">Select Team</option>
        @foreach($teams as $data)
        <option value="{{ $data->id }}">{{ $data->name }}</option>
        @endforeach
        </select>
        
        </td>
        <td>
        <select class="form-control" id="toteam">
        <option value="">Select</option>
        </select>
        </td>
        <td><button class="form-control" id="play_off">Play Now</button></td>
      </tr>
      

      <td>
      
        </td>
    </tbody>
<hr>

   
  </table>

<div id="result">
  <table class="table">
    <thead>
      <tr>
        <th>Win Team</th>
        <th>Lost Team</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td id="winteam"></td>
        <td id="lostteam"></td>
      </tr>
      
    </tbody>

    <thead>
      <tr>
        <th>Score</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td id="score"></td>
      </tr>
      
    </tbody>

  </table>

  </div>



  </div>
<!-- end points table --->


<!-- for points table --->
<div id="total_match" class="tab-pane fade">
  <h4>Total Inning</h4>
  <table class="table">

    <thead>
      <tr style="background-color: #170101;">
        <th style="color: white;">Winner Team</th>
        <th style="color: white;">Lost Team</th>
        <th style="color: white;">Date</th>
      </tr>
    </thead>
    <tbody>
    @foreach($match as $match_key=>$match_val)
      <tr>
      <?php
       $winteam = \App\Teams::where('id', $match_val->win_team)->first();
       $lossteam = \App\Teams::where('id', $match_val->loss_team)->first();
      ?>
        <td >
       {{ $winteam['name'] }}
        </td>


        <td>
        {{ $lossteam['name'] }}
        </td>
      
        <td>
        {{ date_format($match_val->created_at,"Y/m/d") }}
        

        </td>
      
      </tr>
      @endforeach
    </tbody>
<hr>

   
  </table>
  </div>
<!-- end points table --->


  <!-- end tab --->
  </div>

  <script>

$(document).on('change', '#fromteam', function(){
  // alert(this.value);
  var url = '{{ asset("get-team") }}';
  var formdata = {team_id:this.value};
  $.ajax({
  url: url,
  method: "post",
  data:formdata,
  headers: 
  {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  },
  cache: false,
    success: function (json) {              
      // console.log(json);
      $('#toteam').empty();
      var xhtml = '';
      var xhtml = '<option value="">Select Team</option>';
        $.each(json, function (key, val) {
           xhtml = '<option value="'+val.id+'">'+val.name+'</option>';
          
          console.log(val);
            // console.log(json);
            $('#toteam').append(xhtml);
        });    
}
})
})



$(document).on('click', '#play_off', function(){
      //alert(1);
      var url = '{{ asset("play-off-data") }}';
      var play_off = 1;
      var fromteam = $('#fromteam').val();
      var toteam = $('#toteam').val();
      // alert(toteam);
      var formdata = {toteam:toteam,fromteam:fromteam};
      // console.log(formdata);
      $.ajax({
  url: url,
  method: "post",
  data:formdata,
  headers: 
  {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  },
  cache: false,
success: function (json) {  
  alert('Done')
  $('#winteam').html(json.winteam.name);
  $('#lostteam').html(json.lossteam.name);
  console.log(json.lossteam.name);
  console.log(json);
}
})
  })




  </script>
@endsection