<?php

use Illuminate\Database\Seeder;

class TeamsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('teams')->insert([
            'name' => 'Team Australia',
            'logoUri' => 'australia.jpg',
            'clubState' =>'',
        ]);
        DB::table('teams')->insert([
            'name' => 'Team Bangladesh',
            'logoUri' => 'bangladesh.jpg',
            'clubState' =>'',
        ]);
        DB::table('teams')->insert([
            'name' => 'Team England',
            'logoUri' => 'england.jpg',
            'clubState' =>'',
        ]);
        DB::table('teams')->insert([
            'name' => 'Team India',
            'logoUri' => 'india.jpg',
            'clubState' =>'',
        ]);
        DB::table('teams')->insert([
            'name' => 'Team New Zealand',
            'logoUri' => 'new-zealand.jpg',
            'clubState' =>'',
        ]);
        DB::table('teams')->insert([
            'name' => 'Team South Africa',
            'logoUri' => 'south-africa.jpg',
            'clubState' =>'',
        ]);
        DB::table('teams')->insert([
            'name' => 'Team Sri Lanka',
            'logoUri' => 'sri-lanka.jpg',
            'clubState' =>'',
        ]);
        DB::table('teams')->insert([
            'name' => 'Team West Indies',
            'logoUri' => 'west-indies.jpg',
            'clubState' =>'',
        ]);
    }
}
