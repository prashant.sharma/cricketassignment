<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PlayersTableSeeder extends Seeder
{
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
        DB::table('players')->insert(array(
        array(
        'teamsId' => 1,
        'firstName' => 'Aus',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 1,
        'firstName' => 'Aus',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 1,
        'firstName' => 'Aus',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 1,
        'firstName' => 'Aus',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 1,
        'firstName' => 'Aus',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 1,
        'firstName' => 'Aus',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 1,
        'firstName' => 'Aus',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 1,
        'firstName' => 'Aus',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 1,
        'firstName' => 'Aus',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 1,
        'firstName' => 'Aus',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 1,
        'firstName' => 'Aus',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 1,
        'firstName' => 'Aus',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        )
        ));



        DB::table('players')->insert(array(
        array(
        'teamsId' => 2,
        'firstName' => 'Ban',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 2,
        'firstName' => 'Ban',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 2,
        'firstName' => 'Ban',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 2,
        'firstName' => 'Ban',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 2,
        'firstName' => 'Ban',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 2,
        'firstName' => 'Ban',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 2,
        'firstName' => 'Ban',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 2,
        'firstName' => 'Ban',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 2,
        'firstName' => 'Ban',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 2,
        'firstName' => 'Ban',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 2,
        'firstName' => 'Ban',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 2,
        'firstName' => 'Ban',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        )
        ));


        DB::table('players')->insert(array(
        array(
        'teamsId' => 3,
        'firstName' => 'Eng',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 3,
        'firstName' => 'Eng',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 3,
        'firstName' => 'Eng',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 3,
        'firstName' => 'Eng',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 3,
        'firstName' => 'Eng',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 3,
        'firstName' => 'Eng',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 3,
        'firstName' => 'Eng',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 3,
        'firstName' => 'Eng',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 3,
        'firstName' => 'Eng',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 3,
        'firstName' => 'Eng',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 3,
        'firstName' => 'Eng',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 3,
        'firstName' => 'Eng',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        )
        ));

        DB::table('players')->insert(array(
        array(
        'teamsId' => 4,
        'firstName' => 'Ind',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 4,
        'firstName' => 'Ind',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 4,
        'firstName' => 'Ind',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 4,
        'firstName' => 'Ind',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 4,
        'firstName' => 'Ind',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 4,
        'firstName' => 'Ind',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 4,
        'firstName' => 'Ind',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 4,
        'firstName' => 'Ind',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 4,
        'firstName' => 'Ind',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 4,
        'firstName' => 'Ind',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 4,
        'firstName' => 'Ind',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 4,
        'firstName' => 'Ind',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        )
        ));

        DB::table('players')->insert(array(
        array(
        'teamsId' => 5,
        'firstName' => 'NZ',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 5,
        'firstName' => 'NZ',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 5,
        'firstName' => 'NZ',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 5,
        'firstName' => 'NZ',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 5,
        'firstName' => 'NZ',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 5,
        'firstName' => 'NZ',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 5,
        'firstName' => 'NZ',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 5,
        'firstName' => 'NZ',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 5,
        'firstName' => 'NZ',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 5,
        'firstName' => 'NZ',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 5,
        'firstName' => 'NZ',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 5,
        'firstName' => 'NZ',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        )
        ));
        DB::table('players')->insert(array(
        array(
        'teamsId' => 6,
        'firstName' => 'SA',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 6,
        'firstName' => 'SA',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 6,
        'firstName' => 'SA',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 6,
        'firstName' => 'SA',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 6,
        'firstName' => 'SA',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 6,
        'firstName' => 'SA',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 6,
        'firstName' => 'SA',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 6,
        'firstName' => 'SA',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 6,
        'firstName' => 'SA',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 6,
        'firstName' => 'SA',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 6,
        'firstName' => 'SA',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 6,
        'firstName' => 'SA',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        )
        ));

        DB::table('players')->insert(array(
        array(
        'teamsId' => 7,
        'firstName' => 'SL',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 7,
        'firstName' => 'SL',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 7,
        'firstName' => 'SL',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 7,
        'firstName' => 'SL',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 7,
        'firstName' => 'SL',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 7,
        'firstName' => 'SL',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 7,
        'firstName' => 'SL',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 7,
        'firstName' => 'SL',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 7,
        'firstName' => 'SL',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 7,
        'firstName' => 'SL',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 7,
        'firstName' => 'SL',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 7,
        'firstName' => 'SL',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        )
        ));

        DB::table('players')->insert(array(
        array(
        'teamsId' => 8,
        'firstName' => 'WI',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 8,
        'firstName' => 'WI',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 8,
        'firstName' => 'WI',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 8,
        'firstName' => 'WI',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 8,
        'firstName' => 'WI',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 8,
        'firstName' => 'WI',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 8,
        'firstName' => 'WI',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 8,
        'firstName' => 'WI',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 8,
        'firstName' => 'WI',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 8,
        'firstName' => 'WI',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 8,
        'firstName' => 'WI',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        ),
        array(
        'teamsId' => 8,
        'firstName' => 'WI',
        'lastName' => 'Player'.'-'.rand(0,10),
        'jerseyNumber' => rand(0, 12),
        )
        ));
}
}
