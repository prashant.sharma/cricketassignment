<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchinningTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matchinning', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('fromTeamsId')->nullable();
            $table->integer('toTeamsId')->nullable();
            $table->integer('fromPlayerId')->nullable();
            $table->integer('toPlayerId')->nullable();
            $table->string('inning')->nullable();
            $table->integer('over')->nullable();
            $table->integer('ballNumber')->nullable();
            $table->string('typeOfBall')->nullable();
            $table->integer('run')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('matchinning');
    }
}
