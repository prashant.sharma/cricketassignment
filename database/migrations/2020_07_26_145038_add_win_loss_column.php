<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddWinLossColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('matchlog', function (Blueprint $table) {
            $table->integer('matchid')->nullable()->after('id');
            $table->integer('win')->nullable()->after('run');
            $table->integer('loss')->nullable()->after('win');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('matchlog', function (Blueprint $table) {
            //
        });
    }
}
