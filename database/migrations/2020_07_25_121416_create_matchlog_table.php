<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchlogTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matchlog', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('from_team')->nullable();
            $table->integer('to_team')->nullable();
            $table->string('inning')->nullable();
            $table->integer('innig_one_batting')->nullable();
            $table->integer('innig_one_fielding')->nullable();
            $table->integer('number_of_over')->nullable();
            $table->integer('number_of_ball')->nullable();
            $table->integer('wicket')->nullable();
            $table->integer('run')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('match');
    }
}
