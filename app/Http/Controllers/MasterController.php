<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use View;

use DB;

class MasterController extends Controller
{


   public function home(){
     $teams = \App\Teams::get();
      $match = \App\Match::get();
      // dd($match);
      return view('home.home', compact('teams', 'match'));
  }


    public function get_team(Request $request){
      //  return $request->all();
      $data = \App\Teams::select('id', 'name')->whereNotIn('id', [$request->team_id])->get();
      return $data;
    }
    

    public function play_off_data(Request $request){
      //  return $request->all();
   
         $fromteam = \App\Players::select('id')->where('teamsId', $request->fromteam)->take(11)->get();

         foreach($fromteam as $fromkey => $fromval){
            $fromTeamPlayerArr[] = $fromval->id;
         }

         foreach($fromteam as $fromkey => $fromval){
            $fromTeamPlayerArr2[] = $fromval->id;
         }

         $toteam = \App\Players::select('id')->where('teamsId', $request->toteam)->take(11)->get();
         foreach($toteam as $tokey => $toval){
            $toTeamPlayerArr[] = $toval->id;
         }

         $savematch = new \App\Match;
         $savematch->inning_name = 'T20';
         $savematch->fromTeamsId = $request->fromteam;
         $savematch->toTeamsId = $request->toteam;
         $savematch->save();
         $matchid = $savematch->id;


         $innigOneScore = [];
         $innigTwoScore = [];
         $inningWicketOne = 0;
         $inningWicketTwo = 0;
         $innigOneRunScore = 0;
         $innigTwoRunScore = 0;
         $inngWicket1 = 0;
         $inngWicket2 = 0;
         $number_of_ball1 = 0;
         $number_of_ball2 = 0;
         $number_of_over1 = 0;
         $number_of_over2 = 0;
         $inningWicketOne = 0;
         $inningWicketTwo = 0;

            $total_over = 20; // define total overs
            // inning one 
            
            for($i = 1; $i < $total_over; $i++){
               $over = 1;
               foreach($fromTeamPlayerArr as $key => $value){
               // 6 ball per over
               $ballNumber = 1;

               for($j=1; $j<=6;$j++){
                  $run = 0;
                  $type_of_ball = Array("LB","NB","WB","Dot","Y","SB","B","W"); // type of balls

                  $temp_ball = array_rand($type_of_ball); // get random type of ball

                  $array_run = Array('0', '1', '2', '3', '4', '5', '6');  // get random run 

               //  return (array)$fromteam;

                  if($type_of_ball[$temp_ball] == 'W'){
                     $run = 0;
                     $inningWicketOne += 1 -2;
                     $fromTeamPlayerArr = array_slice($fromTeamPlayerArr, 1);
                     
                  }else{
                     $temp_run = array_rand($array_run);
                     $run =  $array_run[$temp_run];
                  }

                  if($type_of_ball[$temp_ball] == 'Dot'){
                     $run = 0;
                  }else{
                     $temp_run = array_rand($array_run);
                     $run =  $array_run[$temp_run];
                  }

                  if($type_of_ball[$temp_ball] == 'WB'){
                     $run = 1 + $array_run[$temp_run];
                  }

                  if($type_of_ball[$temp_ball] == 'NB'){
                     $run = 1 + $array_run[$temp_run];
                  }
               
               if($inningWicketOne < 11){
                  $innigOneRunScore += $run;
                  $match_inning = new \App\MatchInning;
                  $match_inning->matchid = $matchid;
                  $match_inning->fromTeamsId = $request->toteam;
                  $match_inning->toTeamsId = $request->fromteam;
                  $match_inning->fromPlayerId  =  $value;
                  $match_inning->toPlayerId = $toTeamPlayerArr[array_rand($toTeamPlayerArr)]; //$toTeamPlayerArr[$tmp_pid];
                  $match_inning->over = $i;
                  $match_inning->ballNumber = $ballNumber;
                  $match_inning->typeOfBall = $toTeamPlayerArr[array_rand($toTeamPlayerArr)];
                  $match_inning->run = $run;
                  $match_inning->save();
               }

               // count ball and over
               $number_of_ball1 += $ballNumber;
               // $number_of_over1 = 1 + $over;
               //  end
               $ballNumber++;
            }

            
               }
               $over++;      
            
            }

            $innigScore[] = Array(
               'team_id' => $request->fromteam,
               'run' => $innigOneRunScore,
               'wicket' => rand(1,10),
               'number_of_ball' => $number_of_ball1,
               'number_of_over' => $i,
               'innig_one_batting' => $request->fromteam,
               'innig_one_fielding' => $request->toteam,
               'inning' => '2020',
            );

            // return $innigOneScore;
            // inning two

            for($i = 1; $i < $total_over; $i++){
               $over = 1;
               foreach($toTeamPlayerArr as $key => $value2){
               // 6 ball per over
               $ballNumber = 1;

               for($j=1; $j<=6;$j++){
                  $run2 = 0;
                  $type_of_ball = Array("LB","NB","WB","Dot","Y","SB","B","W"); // type of balls

                  $temp_ball = array_rand($type_of_ball); // get random type of ball

                  $array_run = Array('0', '1', '2', '3', '4', '5', '6');  // get random run 

               //  return (array)$fromteam;

                  if($type_of_ball[$temp_ball] == 'W'){
                     $run2 = 0;
                     $inningWicketTwo += 1 - 2;
                     $toTeamPlayerArr = array_slice($toTeamPlayerArr, 1);
                     
                  }else{
                     $temp_run = array_rand($array_run);
                     $run2 =  $array_run[$temp_run];
                  }

                  if($type_of_ball[$temp_ball] == 'Dot'){
                     $run2 = 0;
                  }else{
                     $temp_run = array_rand($array_run);
                     $run2 =  $array_run[$temp_run];
                  }

                  if($type_of_ball[$temp_ball] == 'WB'){
                     $run2 = 1 + $array_run[$temp_run];
                  }

                  if($type_of_ball[$temp_ball] == 'NB'){
                     $run2 = 1 + $array_run[$temp_run];
                  }

               if($inningWicketTwo < 11){
                  $innigTwoRunScore += $run2;
                  $match_inning = new \App\MatchInning;
                  $match_inning->matchid = $matchid;
                  $match_inning->fromTeamsId = $request->toteam;
                  $match_inning->toTeamsId = $request->fromteam;
                  $match_inning->fromPlayerId  =  $value2;
                  $match_inning->toPlayerId = $fromTeamPlayerArr2[array_rand($fromTeamPlayerArr2)]; //$fromTeamPlayerArr2[$tmp_pid];
                  $match_inning->over = $i;
                  $match_inning->ballNumber = $ballNumber;
                  $match_inning->typeOfBall = $fromTeamPlayerArr2[array_rand($fromTeamPlayerArr2)];
                  $match_inning->run = $run2;
                  $match_inning->save();
               }

               // count ball and over
               $number_of_ball2 += $ballNumber;
               $ballNumber++;
            }

            
               }
               $over++;      
            
            }

            $innigScore[] = Array(
               'team_id' => $request->toteam,
               'run' => $innigTwoRunScore,
               'wicket' => rand(1,10),
               'number_of_ball' => $number_of_ball2,
               'number_of_over' => $i,
               'innig_one_batting' => $request->toteam,
               'innig_one_fielding' => $request->fromteam,
               'inning' => '2020',
            );

         


            foreach($innigScore as $data){
               // return $data['innig_one_batting'];
               // $data['wicket']

               $savematchlog = new \App\MatchLog;
               $savematchlog->teamsid = $data['team_id'];
               $savematchlog->matchid = $matchid;
               $savematchlog->from_team = $request->fromteam;
               $savematchlog->to_team = $request->toteam;
               $savematchlog->inning = $data['inning'];
               $savematchlog->innig_one_batting = $data['innig_one_batting'];
               $savematchlog->innig_one_fielding = $data['innig_one_fielding'];
               $savematchlog->number_of_over = $data['number_of_over'];
               $savematchlog->number_of_ball = $data['number_of_ball'];
               $savematchlog->wicket = $data['wicket'];
               $savematchlog->run = $data['run'];
               $savematchlog->save();
            }
            

            // return $innigScore['0']['run'];

         if($innigScore[0]['run'] >> $innigScore[1]['run']){
            $arr = Array(
              'win' => $request->fromteam,
              'loss' => $request->toteam,
            );
         }else{
            $arr = Array(
               'win' => $request->toteam,
               'loss' => $request->fromteam,
             );
         }

         // return $arr;

         // add match results
         $updatematch = \App\Match::where('id', $matchid)->first();
         $updatematch->win_team = $arr['win'];
         $updatematch->loss_team = $arr['loss'];
         $updatematch->update();


         $winteam = \App\Teams::where('id', $updatematch->win_team)->first();
         $lossteam = \App\Teams::where('id', $updatematch->loss_team)->first();

         $tmparr = \App\MatchLog::where('matchid', $matchid)->get();
         foreach ($tmparr as $key => $value) {
            $ingdata[] = Array( 'run' => $value->run, 'wicket' => $value->wicket );
         }

         $data = Array(
            'winteam' => $winteam,
            'lossteam' => $lossteam,
            'score' => $ingdata
         );
        return $data;
         
         // return $arr;
    }
}
